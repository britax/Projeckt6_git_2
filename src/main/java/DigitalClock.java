

import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;


public class DigitalClock extends Parent {

    private HBox hbox = new HBox();
    private Text[] digits = new Text[2];
    private Group[] digitsGroup = new Group[2];


    public DigitalClock() {
        configure();
        getChildren().addAll(hbox);
    }


    private void configure() {
        for (int i = 0; i < digits.length; i++) {
            digits[i] = new Text("0");
            digits[i].setFont(new Font(50));
            digitsGroup[i] = new Group(digits[i]);
        }

        hbox.getChildren().addAll(digitsGroup);
        hbox.setSpacing(2);
    }

    public void refreshDigits(String number) {
        for (int i = 0; i < digits.length; i++) {
            digits[i].setText(number.substring(i, i + 1));
        }
    }


}























