import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Label;


public class Alert {

    Stage window = new Stage();

    public void display(String tilte, String text){

        Label label =  new Label();
        label.setText(text);
        Button closeButton =  new Button("Close");
        closeButton.setOnAction(e-> window.close());

        VBox vBox = new VBox(10);
        vBox.getChildren().addAll(label, closeButton);
        vBox.setAlignment(Pos.CENTER);


        Scene scene = new Scene(vBox);
        window.setScene(scene);
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(tilte);
        window.setMinWidth(100);
        window.showAndWait();

    }



}
