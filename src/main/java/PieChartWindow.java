import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.chart.*;
import javafx.scene.Group;




public class PieChartWindow {
    Stage window = new Stage();
    PieChartData allPieChartData = new PieChartData(DevelopersDiary.username);

    public void start() {

        final PieChart chart = new PieChart(allPieChartData.getPieChartData());
        chart.setTitle("Arbeitszeit ");

        final Label caption = new Label("");
        caption.setStyle("-fx-font: 24 arial;");

        for (final PieChart.Data data : chart.getData()) {
            data.getNode().addEventHandler(MouseEvent.MOUSE_PRESSED,
                    e -> {
                        double total = 0;
                        for (PieChart.Data d : chart.getData()) {
                            total += d.getPieValue();
                        }
                        caption.setTranslateX(e.getSceneX());
                        caption.setTranslateY(e.getSceneY());
                        String text = String.format("%.1f%%", 100*data.getPieValue()/total) ;
                        caption.setText(text);
                    }
            );
        }

        Button buttonClose = new Button("Close");
        buttonClose.setOnAction(e -> {
            close();
        });

        VBox vBox = new VBox();
        vBox.setSpacing(10);
        vBox.setAlignment(Pos.CENTER);
        vBox.getChildren().addAll(chart, buttonClose);

        Scene scene = new Scene(new Group());
        ((Group) scene.getRoot()).getChildren().addAll(vBox, caption);
        window.setScene(scene);

        window.setTitle("Kuchen Diagramm");
        window.setWidth(500);
        window.setHeight(500);
        window.setOnCloseRequest(e -> close());
        window.show();
    }

    private void close(){
        window.close();
        allPieChartData.delete();
    }


}