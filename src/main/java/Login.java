import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Label;


public class Login {
    Stage window = new Stage();

    public String display(){


        Label label =  new Label();
        label.setText("Bitte Username Einfügen");

        TextField inputUsername = new TextField();
        inputUsername.setPromptText("Username");

        Button closeButton =  new Button("Login");
        closeButton.setOnAction(e-> {
            if (inputUsername.getText().equals("")) {
                Alert alert= new Alert();
                alert.display("Error", "Bitte Username eingeben");
            }else{
                window.close();
            }
        });

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label,inputUsername, closeButton);
        layout.setAlignment(Pos.CENTER);

        Scene scene = new Scene(layout);
        window.setScene(scene);

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Login");
        window.setMinWidth(100);
        window.showAndWait();
        return inputUsername.getText();




    }



}
