import impl.FetchProjectsFromServer;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class EditWindow {

    private Stage window = new Stage();
    static List<String[]> choiceBoxProjects = new ArrayList<>();

    public void display(long id, ObservableList<ListItem> listItems) {


        configure((int) id, listItems);
        window.show();

    }

    private void configure(int id, ObservableList<ListItem> listItems) {
        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        Label lableProject = new Label("Projekt:");
        GridPane.setConstraints(lableProject, 0, 0);

        FetchProjectsFromServer get = new FetchProjectsFromServer();
        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        fillChoiceBox(choiceBox, get);
        choiceBox.setValue(listItems.get(id).getProject());

        Label lableInternal = new Label("");
        internalSetter(lableInternal, choiceBox);

        choiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
            internalSetter(lableInternal, choiceBox);
        });

        HBox hBox = new HBox(10);
        hBox.getChildren().addAll(choiceBox, lableInternal);
        GridPane.setConstraints(hBox, 1, 0);

        Label lableTitel = new Label("Title:");
        GridPane.setConstraints(lableTitel, 0, 1);

        TextField inputTitle = new TextField(listItems.get(id).getSubject());
        inputTitle.setPromptText("Title");
        inputTitle.setPrefWidth(410);
        GridPane.setConstraints(inputTitle, 1, 1);

        Label lableDetail = new Label("Details:");
        GridPane.setConstraints(lableDetail, 0, 2);

        TextArea inputDetail = new TextArea(listItems.get(id).getDescription());
        inputDetail.setPromptText("Details");
        inputDetail.setPrefHeight(400);
        inputDetail.setPrefWidth(410);
        GridPane.setConstraints(inputDetail, 1, 2);

        Label TimeLable = new Label("Start:  " + listItems.get(id).getStart() + "  Ende: " + listItems.get(id).getEnd());
        GridPane.setConstraints(TimeLable, 1, 3);

        Button buttonSave = new Button("Save");
        buttonSave.setOnAction(e -> {
            saveFunktion(id, listItems, choiceBox, inputTitle, inputDetail);
        });

        Button buttonDelet = new Button("Delete");
        buttonDelet.setOnAction(e -> {
            deleteFunction(id, listItems);
        });

        HBox hBoxButtons = new HBox(10);
        hBoxButtons.getChildren().addAll(buttonSave, buttonDelet);

        GridPane.setConstraints(hBoxButtons, 1, 4);
        grid.getChildren().addAll(lableProject, hBox, lableTitel, inputTitle, lableDetail, inputDetail, hBoxButtons, TimeLable);

        Scene scene = new Scene(grid, 500, 300);
        window.setScene(scene);

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Eintrag bearbeiten");
        window.setOnCloseRequest(e -> close());
    }

    private void internalSetter(Label lableInternal, ChoiceBox<String> choiceBox) {
        for (int i = 0; i < choiceBoxProjects.size(); i++) {
            if (choiceBoxProjects.get(i)[0].equals(choiceBox.getValue())) {
                lableInternal.setText(choiceBoxProjects.get(i)[1]);
            }
        }
    }

    private void saveFunktion(int id, ObservableList<ListItem> listItems, ChoiceBox<String> choiceBox, TextField inputTitle, TextArea inputDetail) {
        EntryManager.update(listItems.get(id).getGlobaleId(), EntryManager.selectProject(choiceBox.getValue()), DevelopersDiary.username, inputTitle.getText(), inputDetail.getText());
        close();
    }

    private void deleteFunction(int id, ObservableList<ListItem> listItems) {
        EntryManager.delete(listItems.get(id).getGlobaleId());
        close();
    }

    private void close() {
        window.close();
        ListWindow l = new ListWindow();
        l.go();
    }

    private static void fillChoiceBox(ChoiceBox<String> choiceBox, FetchProjectsFromServer get) {
        String internal;
        for (int i = 0; i < get.fetch().size(); i++) {
            if (get.fetch().get(i).isInternal()) {
                internal = " (Intern)";
            } else {
                internal = " (Extern)";
            }

            choiceBoxProjects.add(new String[]{get.fetch().get(i).getName(), internal});
            choiceBox.getItems().add(get.fetch().get(i).getName());
        }
    }


}
