import diary.DiaryEntry;
import diary.Project;
import impl.FetchProjectsFromServer;
import impl.FileStore;
import java.time.LocalDateTime;
import java.util.List;

public class EntryManager {

    public static void save(Project project, String username, String subject, String description, LocalDateTime start, LocalDateTime end) {
        diary.DiaryEntry diaryEntry = new diary.DiaryEntry(project, username, subject, description, start, end);
        FileStore fileStore = new FileStore();

        List<DiaryEntry> diaryEntries = fileStore.read();

        if (diaryEntries == null) {
            fileStore.write((List<DiaryEntry>) diaryEntry);
        } else {
            diaryEntries.add(diaryEntry);
            fileStore.write(diaryEntries);
        }

    }

    public static void delete(long id) {
        FileStore fileStore = new FileStore();
        List<DiaryEntry> diaryEntries = fileStore.read();

        diaryEntries.remove((int) id);

        fileStore.write(diaryEntries);

    }

    public static void update(long id, Project project, String username, String subject, String description) {
        FileStore fileStore = new FileStore();
        List<DiaryEntry> diaryEntries = fileStore.read();

        diary.DiaryEntry diaryEntry = new diary.DiaryEntry(project, username, subject, description, diaryEntries.get((int)id).getStart(), diaryEntries.get((int)id).getEnd());

        diaryEntries.set((int)id, diaryEntry);
        fileStore.write(diaryEntries);


    }

    public static Project selectProject(String name) {
        FetchProjectsFromServer fetchProjectsFromServer = new FetchProjectsFromServer();
        List<Project> list = fetchProjectsFromServer.fetch();
        if (list == null) {
            return null;
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getName().equals(name)) {
                return list.get(i);
            }
        }
        return null;

    }



}
