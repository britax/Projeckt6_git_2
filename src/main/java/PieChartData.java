import diary.DiaryEntry;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import java.util.ArrayList;
import java.util.List;

public class PieChartData {

    static ArrayList<String> names = new ArrayList<>();
    static ArrayList<Long> values = new ArrayList<>();

    private static void add(String name, long value){
        names.add(name);
        values.add(value);
    }

    public PieChartData(String username){
        impl.FileStore fileStore = new impl.FileStore();
        List<DiaryEntry> diaryEntries = fileStore.read();
        for (int i = 0; i<diaryEntries.size(); i++){
            if(diaryEntries.get(i).getUsername().equals(username)){
                int index = projectIsAvailable(diaryEntries.get(i).getProject().getName());
                if (index != -1){
                    values.set(index, values.get(index)+ diaryEntries.get(i).computeDuration());
                }else {
                    add(diaryEntries.get(i).getProject().getName(), diaryEntries.get(i).computeDuration());
                }
            }
        }
        for (int i = 0; i < values.size(); i++){
            if (values.get(i) < 1){
                values.set(i, (long)1);

            }
        }

    }
    public void delete(){
        names = new ArrayList<>();
        values = new ArrayList<>();
    }

    private int projectIsAvailable(String project){
        if (names == null){
            return -1;
        }
        for (int i =0; i < names.size(); i++ ){
            if (names.get(i).equals(project)){
                return i;
            }
        }
        return -1;
    }

    public ObservableList<PieChart.Data> getPieChartData(){
        PieChart.Data[] pieChart = new PieChart.Data[names.size()];

        for (int i =0; i < names.size(); i++){
            pieChart[i] = new PieChart.Data(names.get(i), values.get(i));
        }
        return FXCollections.observableArrayList(pieChart);
    }
}
