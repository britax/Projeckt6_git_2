import impl.FetchProjectsFromServer;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class NewEntryWindow {
    Stage window = new Stage();
    static List<String[]> choiceBoxProjects = new ArrayList<>();

    public void display(LocalDateTime startTime, LocalDateTime endTime) {

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Neuer Eintrag");


        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(8);
        grid.setHgap(10);

        Label lableProject = new Label("Projekt:");
        GridPane.setConstraints(lableProject, 0, 0);

        Label lableInternal = new Label("");


        ChoiceBox<String> choiceBox = new ChoiceBox<>();
        FetchProjectsFromServer get = new FetchProjectsFromServer();
        fillChoiceBox(choiceBox, get);
        GridPane.setConstraints(choiceBox, 1, 0);
        HBox hBox = new HBox(10);
        choiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
            for (int i = 0; i < choiceBoxProjects.size(); i++) {
                if (choiceBoxProjects.get(i)[0].equals(choiceBox.getValue())) {
                    lableInternal.setText(choiceBoxProjects.get(i)[1]);
                }
            }

        });
        hBox.getChildren().addAll(choiceBox, lableInternal);
        GridPane.setConstraints(hBox, 1, 0);


        Label lableTitel = new Label("Title:");
        GridPane.setConstraints(lableTitel, 0, 1);

        TextField inputTitle = new TextField();
        inputTitle.setPromptText("Title");
        inputTitle.setPrefWidth(410);
        GridPane.setConstraints(inputTitle, 1, 1);

        Label lableDetail = new Label("Details:");
        GridPane.setConstraints(lableDetail, 0, 2);

        TextArea inputDetail = new TextArea();
        inputDetail.setPromptText("Details");
        inputDetail.setPrefHeight(400);
        inputDetail.setPrefWidth(410);
        GridPane.setConstraints(inputDetail, 1, 2);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        Label TimeLable = new Label("Start:  " + startTime.format(formatter) + "  Ende: " + endTime.format(formatter));
        GridPane.setConstraints(TimeLable, 1, 3);


        Button buttonSave = new Button("Save");
        buttonSave.setOnAction(e -> {
            save(startTime, endTime, window, choiceBox, inputTitle, inputDetail);
        });


        GridPane.setConstraints(buttonSave, 1, 4);

        grid.getChildren().addAll(lableProject, hBox, lableTitel, inputTitle, lableDetail, inputDetail, buttonSave, TimeLable);
        Scene scene = new Scene(grid, 500, 300);
        window.setScene(scene);

        window.showAndWait();
    }

    private static void save(LocalDateTime startTime, LocalDateTime endTime, Stage window, ChoiceBox<String> choiceBox, TextField inputTitle, TextArea inputDetail) {
        if (EntryManager.selectProject(choiceBox.getValue()) == null) {
            Alert alert = new Alert();
            alert.display("Error", "Es ist kein Projekt vorhanden");
        } else if (inputTitle.getText().length() < 300 && !inputTitle.getText().equals("")) {
            EntryManager.save(EntryManager.selectProject(choiceBox.getValue()), DevelopersDiary.username, inputTitle.getText(), inputDetail.getText(), startTime, endTime);
            window.close();
        } else {
            Alert alert = new Alert();
            alert.display("Error", "Der Title ist zu kurz oder zu lang");
        }
    }

    private static void fillChoiceBox(ChoiceBox<String> choiceBox, FetchProjectsFromServer get) {
        String internal;
        for (int i = 0; i < get.fetch().size(); i++) {
            if (get.fetch().get(i).isInternal()) {
                internal = " (Intern)";
            } else {
                internal = " (Extern)";
            }

            choiceBoxProjects.add(new String[]{get.fetch().get(i).getName(), internal});
            choiceBox.getItems().add(get.fetch().get(i).getName());
        }

    }


}
