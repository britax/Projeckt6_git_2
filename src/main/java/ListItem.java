import diary.DiaryEntry;

import java.time.format.DateTimeFormatter;

public class ListItem {
    private long id;
    private long globaleId;
    private final String username;
    private final String subject;
    private final String description;
    private final String start;
    private final String end;
    private final String project;
    private final String internal;

    public ListItem(long id, long globaleId, DiaryEntry diaryEntry) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        this.id = id;
        this.globaleId = globaleId;
        this.username = diaryEntry.getUsername();
        this.subject = diaryEntry.getSubject();
        this.description = diaryEntry.getDescription();
        this.start = diaryEntry.getStart().format(formatter);
        this.end = diaryEntry.getEnd().format(formatter);
        this.project = diaryEntry.getProject().getName();
        if(diaryEntry.getProject().isInternal()) {
            this.internal = "Intern";
        }else{
            this.internal = "Extern";
        }

    }


    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }


    public String getSubject() {
        return subject;
    }

    public String getDescription() {
        return description;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getInternal() {
        return internal;
    }

    public String getProject() {
        return project;
    }

    public long getGlobaleId() {
        return globaleId;
    }
}
