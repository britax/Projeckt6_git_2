import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.Scene;


public class StartWindow extends Application {
    Stage window;
    Label label;

    public void go() {
        launch();
    }

    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Developers Diary");

        HBox hBox = new HBox(10);
        Button buttonNewEntry = new Button("Neuer Eintrag");
        buttonNewEntry.setOnAction(e -> {
            TimeWindow t = new TimeWindow();
            t.go();

        });

        Button buttonOverview = new Button("Übersicht");
        buttonOverview.setOnAction(e-> {
            ListWindow l = new ListWindow();
            l.go();

        });

        Button buttonPieChart = new Button("Diagramm");
        buttonPieChart.setOnAction(e-> {
            PieChartWindow pieChartWindow = new PieChartWindow();
            pieChartWindow.start();

        });

        Button newLoginButton = new Button("Neu einloggen");
        newLoginButton.setOnAction(e-> {
            startLogin();
        });

        label = new Label("Bitte Anmelden");
        VBox vBox=  new VBox(5);
        vBox.getChildren().addAll(label,newLoginButton );
        vBox.setAlignment(Pos.CENTER);


        hBox.getChildren().addAll(buttonNewEntry, buttonOverview, buttonPieChart);
        hBox.setAlignment(Pos.CENTER);

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(hBox);
        borderPane.setCenter(vBox);

        Scene starten = new Scene(borderPane, 300, 250);
        window.setScene(starten);
        window.show();
        startLogin();
        if (DevelopersDiary.username.equals("")){
            window.close();
        }

    }

    public void startLogin(){
        Login login =  new Login();
        DevelopersDiary.username = login.display();
        label.setText("Sie sind angemeldet als: "+DevelopersDiary.username);
    }

}
