import java.text.DecimalFormat;
import java.time.LocalDateTime;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TimeWindow {

    private VBox vbox = new VBox();
    private Label label = new Label("Verstrichene Minuten");
    public DigitalClock clock = new DigitalClock();
    public boolean running = true;
    static Stage window = new Stage();


    private LocalDateTime startTime = LocalDateTime.now();
    public LocalDateTime endTime;

    public void go() {
        Button buttonStop = new Button("Stopp");
        buttonStop.setOnAction(e -> close());

        vbox.getChildren().addAll(label, clock, buttonStop);
        vbox.setAlignment(Pos.CENTER);

        Scene scene = new Scene(vbox);
        window.setScene(scene);
        window.setOnCloseRequest(e -> close());
        window.show();

        runClock();

    }


    private void runClock() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                long last = System.nanoTime();
                double delta = 0;
                double ns = 1000000000.0 / 1*60;
                int count = 0;

                while (running) {
                    if (count == 30) {
                        running = false;
                        endTime = LocalDateTime.now();
                        Runnable r = () -> {
                            Alert alert = new Alert();
                            alert.display("Zeitende", "Die Zeit wurde angehalten." + System.getProperty("line.separator") + "Bitte kommen sie zum Ende");
                        };
                        Platform.runLater(r);

                    }
                    long now = System.nanoTime();
                    delta += (now - last) / ns;
                    last = now;

                    while (delta >= 1) {
                        count = (count + 1) % 60;
                        DecimalFormat df = new DecimalFormat("00");
                        clock.refreshDigits(df.format(count));
                        delta--;
                    }
                }
            }
        };
        thread.start();
    }

    public void close() {
        running = false;
        window.close();
        if (endTime == null) {
            endTime = LocalDateTime.now();
        }
        NewEntryWindow entryWindow = new NewEntryWindow();
        entryWindow.display(startTime, endTime);
    }

}



